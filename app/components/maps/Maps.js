import React, {Component} from 'react';
import MapView, {PROVIDER_GOOGLE, Marker, Callout} from 'react-native-maps'
import {Text, View, Button,Switch, StyleSheet,TouchableOpacity, ScrollView,TextInput, TouchableWithoutFeedback,Dimensions} from 'react-native';

const width =  Dimensions.get('window').width
const height = Dimensions.get('window').height
export default class Maps extends Component {
    constructor(props){
        super(props);
        this.state = {
            places: [
                {
                    name:'Tecsup Norte',
                    coordinates: {
                        latitud: -8.1494049,
                        longitud: -79.0437666,
                    },
                },
                {
                    name:'Tecsup Centro',
                    coordinates: {
                        latitud: -12.0447884,
                        longitud: -76.9545117,
                   },
                },
                {
                    name:'Tecsup Sur',
                    coordinates: {
                        latitud: -16.4288508,
                        longitud: -71.5038502,
                    },
                },
            ],
            latitude:0,
            longitude: 0,
        };
    }
    onMapLayout = () => {
        this.setState({ isMapReady: true });
      }

    render(){
        return (
            <View style={styles.container}>
                <MapView
                style={styles.map}
                provider={PROVIDER_GOOGLE}
                initialRegion={{
                    latitude: -16.409046,
                    longitude: -71.537453,
                    latitudeDelta: 0.0922,
                    longitudeDelta: 0.0421,
                }}>
                { 
                this.state.places.map((item, index)=>{
                    return (
                        <Marker
                            key={index}
                            coordinate={{
                                latitude: item.coordinates.latitud,
                                longitude: item.coordinates.longitud,
                            }}>
                            <Callout>
                                <View
                                    style={{
                                        backgroundColor: "#FFFFFF",
                                        borderRadius: 5,
                                    }}>
                                    <Text>
                                        {item.name}
                                    </Text>
                                </View>
                            </Callout>
                        </Marker>
                    );
                })}    

                </MapView>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
      },
    map: {
        
        width: width,
        height: height
    },
});
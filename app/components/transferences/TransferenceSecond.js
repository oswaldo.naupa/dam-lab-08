var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View,TouchableOpacity,ScrollView, Button,Dimensions,StyleSheet,Alert} from 'react-native';

const { width } = Dimensions.get("window");
const ancho = width -120;


export default class TransferenceSecond extends Component {
    constructor(props){
        super(props);
        this.state = {
            importe: 0,
        };
    }
    
    showAlert = () =>{
      Alert.alert(
        'Confirmacion de Envio',
        '¿Esta seguro que los datos son correctos?',
        [
          {
            text:'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style:'cancel',
          },
          {text:'OK', onPress:() => this.props.navigation.navigate('Third')}
        ],
        {cancelable: false},
      );
    };

    render() {
        const { cuentaOrigen } = this.props.route.params;
        const { cuentaDestino } = this.props.route.params;
        const { importe } = this.props.route.params;
        const { referencia } = this.props.route.params;
        const { fecha } = this.props.route.params;
        const { sendMail } = this.props.route.params;
        var email =""
        if (sendMail) {
            email = "Si"
        }else{
            email = "No"
        }
        return(
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.colum}>
                <Text style={styles.text}>Cuenta Origen </Text>
                <Text style={styles.textresult}>{cuentaOrigen}</Text>
                <View style={styles.separador}/>
                <Text style={styles.text}>Cuenta Destino </Text>
                <Text style={styles.textresult}>{cuentaDestino}</Text>
                <View style={styles.separador}/>
                <Text style={styles.text}>Importe </Text>
                <Text style={styles.textresult}>{importe}</Text>
                <View style={styles.separador}/>
                <Text style={styles.text}>Referencia </Text>
                <Text style={styles.textresult}>{referencia}</Text>
                <View style={styles.separador}/>
                <Text style={styles.text}>Fecha </Text>
                <Text style={styles.textresult}>{fecha}</Text>
                <View style={styles.separador}/>
                <Text style={styles.text}>Email </Text>
                <Text style={styles.textresult}>{email}</Text>
                <View style={styles.separador}/>
                <View style={styles.containerboton}>
                    <View style={styles.button}> 
                        <TouchableOpacity style={styles.buttoncolor}  onPress={()=>this.props.navigation.navigate('First')}>
                            <Text style={styles.textbuton}> VOLVER </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.button}> 
                        <TouchableOpacity style={styles.buttoncolor}  onPress={()=>this.showAlert()}>
                            <Text style={styles.textbuton}> CONFIRMAR </Text>   
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginBottom: 5,
      alignItems: 'center',
      padding:60
    },
    colum: {
      flexDirection:'column', alignSelf:'stretch'
    },
    button: {
      width: ancho*0.5,
      alignSelf: "center",
      margin:10
      
    },
    containerboton: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
      },
    touch:{
      alignSelf: "center",
      height: 40,
      borderRadius: 6, 
      backgroundColor: '#dcdcdc',
      width: ancho*0.3,
      margin:15,
      flexDirection: 'row',alignItems: 'center'
    },
    input:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'#dcdcdc',
    },
    inputmodal:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      backgroundColor:'#dcdcdc',
      textAlign: 'center',
      fontSize: 15,
    },

    inputref:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'red',
      
    },
    date:{
      textAlign: 'center', // <-- the magic
      fontWeight: 'bold',
      fontSize: 15,
      textAlignVertical: "center",
      width: '100%'
    },
    text:{
      fontSize: 15,
      margin:4,
      width:ancho,
    },
    textresult:{
        fontSize: 20,
        margin:4,
        width:ancho,
        borderBottomColor:'red'
    },
    separador:{
        height: 2, 
        width: "100%",
        backgroundColor: "#a9a9a9",
    },
    buttoncolor:{
        backgroundColor:'#000080',
        height: 40,
        borderRadius: 6,
        flexDirection: 'row',alignItems: 'center' 
      },
      textbuton:{
        textAlign: 'center', // <-- the magic
        fontWeight: 'bold',
        fontSize: 15,
        textAlignVertical: "center",
        width: '100%',
        color: '#ffffff'
      }
  })
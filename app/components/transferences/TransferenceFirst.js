var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button,Switch, StyleSheet,TouchableOpacity, ScrollView,TextInput, TouchableWithoutFeedback,Dimensions} from 'react-native';
import ModalSelector from 'react-native-modal-selector';
import DateTimePicker from "react-native-modal-datetime-picker";
const { width } = Dimensions.get("window");
const ancho = width -120;
export default class TransferenceFirst extends Component {
    
  constructor(props){
        super(props);
        this.state = {
            importe: '',
            referencia:'',
            isDateTimePickerVisible: false,
            fecha: ("0" + new Date().getDate()).slice(-2)+"/"+("0" + (new Date().getMonth() + 1)).slice(-2)+"/"+new Date().getFullYear(),
            sendMail: false,
            cuentaOrigen: 'Select somethings',
            cuentaDestino: 'Select somethings',
            mensajerrororigen:'',
            mensajerrordestino:'',
            mensajerrornumero:'',
            mensajeerrorreferencia:'',
            colorimporte: '#dcdcdc',
            colorreferencia: '#dcdcdc',
            colorcuentaorigen: '#dcdcdc',
            colorcuentadestino:'#dcdcdc'
        };
    }
    changeImporte = text => {
      let newText = '';
      let numbers = '0123456789';
  
      for (var i=0; i < text.length; i++) {
        if(numbers.indexOf(text[i]) > -1 ) {
            newText = newText + text[i];
            this.setState({ mensajerrornumero: "",colorimporte:"#66cdaa"});
        }
        else {
            // your call back function
            newText = newText + text[i];
            this.setState({ mensajerrornumero: "Solo debe ingresar letras",colorimporte:"#ff4500"});
        }
    }
    this.setState({ importe: newText });
    };

    /*changeImporte = importe => {
      this.setState({importe});
    };*/
    changeReferencia= referencia => {
      this.setState({referencia,mensajeerrorreferencia:'',colorreferencia:'#66cdaa'});
    };

    changeOriginAcount=cuentaOrigen=>{
      this.setState({cuentaOrigen,mensajerrororigen:"",colorcuentaorigen:'#66cdaa'})
    }
    changeDestinationAcount=cuentaDestino=>{
      this.setState({cuentaDestino,mensajerrordestino:"",colorcuentadestino:'#66cdaa'})
    }

    showDateTimePicker = () => {
      this.setState({isDateTimePickerVisible:true});
    };
   
    hideDateTimePicker = () => {
      this.setState({isDateTimePickerVisible: false});
    };
   
    handleDatePicked= (date) => {
      console.warn("A date has been picked: ", date);
      
      this.setState({
        fecha: moment (date) .format('DD/MM/YYYY'),
        fechaFormat: moment(date) .format('YY/MM/DD'),
      });

      this.hideDateTimePicker();
    };
    async naa(){
      await this.validator();
      if(this.state.mensajeerrorreferencia==='' && this.state.mensajerrororigen===''&&this.state.mensajerrordestino===''&&this.state.mensajerrornumero===''){
        this.props.navigation.navigate('Second', {
          cuentaOrigen: this.state.cuentaOrigen,
          cuentaDestino: this.state.cuentaDestino,
          importe: this.state.importe,
          referencia: this.state.referencia,
          fecha: this.state.fecha,
          sendMail: this.state.sendMail
      })

    }
  }
  validator () {
      if(this.state.cuentaOrigen==="Select somethings"){
        this.setState({mensajerrororigen:"Debe elegir una opcion", colorcuentaorigen:'#ff4500'})
      }
      if(this.state.cuentaDestino==="Select somethings"){
        this.setState({mensajerrordestino:"Debe elegir una opcion",colorcuentadestino:'#ff4500'})
      }
      if(this.state.importe===''){
        this.setState({mensajerrornumero:"Debe ingresar un monto", colorimporte:'#ff4500'})
      }
      if(this.state.referencia===''){
        this.setState({mensajeerrorreferencia:"Debe ingresar una referencia", colorreferencia:'#ff4500'})
      }
      

      /*else{
        this.props.navigation.navigate('Second', {
          cuentaOrigen: this.state.cuentaOrigen,
          cuentaDestino: this.state.cuentaDestino,
          importe: this.state.importe,
          referencia: this.state.referencia,
          fecha: this.state.fecha,
          sendMail: this.state.sendMail
      })}*/
    }

    render() {
      

      let index = 0;
      const data = [
          { key: index++, label: '000000000231456' },
          { key: index++, label: '000000000287565' },
          { key: index++, label: '000000000245657' },
      ];

        return(
          <ScrollView>
            <View style={styles.container}>
              <View style={styles.colum}>
                <Text style={styles.text}>Cuenta origen</Text>
                <ModalSelector
                  
                  data={data}
                  initValue={this.state.cuentaOrigen}
                  cancelText="Cancelar"
                  onChange={option => {
                    this.changeOriginAcount(option.label);
                  }}>

                  <TextInput
                        style={[{borderColor:this.state.colorcuentaorigen},styles.inputmodal]}
                        editable={false}
                        value={this.state.cuentaOrigen} />

                  </ModalSelector>
                  { this.state.mensajerrororigen != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajerrororigen}</Text>:<View></View>}
              
                
                <Text style={styles.text}>Cuenta destino</Text>
                <ModalSelector
                  data={data}
                  initValue={this.state.cuentaDestino}
                  cancelText="Cancelar"
                  onChange={option => {
                    this.changeDestinationAcount(option.label);
                  }}>
                    <TextInput
                        style={[{borderColor:this.state.colorcuentadestino},styles.inputmodal]}
                        editable={false}
                        value={this.state.cuentaDestino} />
                  </ModalSelector>
                 { this.state.mensajerrordestino != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajerrordestino}</Text>:<View></View>}

                <Text style={styles.text}>Importe</Text>
                <TextInput
                  underlineColorAndroid={'transparent'}
                  style={[{borderColor:this.state.colorimporte},styles.input]}
                
                  onChangeText= {importe=>this.changeImporte(importe)}
                  value={this.state.importe}
                />
                { this.state.mensajerrornumero != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajerrornumero}</Text>:<View></View>}

                <Text style={styles.text}>Referencia</Text>
                <TextInput
                  underlineColorAndroid={'transparent'}
                  style={[{borderColor:this.state.colorreferencia},styles.inputref]}
                  onChangeText= {referencia=>this.changeReferencia(referencia)}
                />
                { this.state.mensajeerrorreferencia != "" ? 
                  <Text style={styles.texterror}>{this.state.mensajeerrorreferencia}</Text>:<View></View>}

                <TouchableWithoutFeedback onPress={this.showDateTimePicker}>
                  <View style={styles.touch}>
                    <Text style={styles.date}>
                      {this.state.fecha}
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <DateTimePicker
                  isVisible={this.state.isDateTimePickerVisible}
                  onConfirm={this.handleDatePicked}
                  onCancel={this.hideDateTimePicker}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    marginTop: 5,
                  }}
                >
                <Text>Notificarme al email</Text>
                <Switch
                  onValueChange={value=>this.setState({sendMail: value})}
                  trackColor={{ false: "#767577", true: "#000080" }}
                  thumbColor={this.state.sendMail ? "#f4f3f4" : "#f4f3f4"}
                  value={this.state.sendMail}
                ></Switch>  
                </View>
                <View style={styles.button}> 
                  <TouchableOpacity style={styles.buttoncolor}  title="Siguiente" onPress={()=>this.naa()}>
                    <Text style={styles.textbuton}> SIGUIENTE </Text>
                  </TouchableOpacity>
                  </View>
               
              </View>
            </View>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
      flexDirection: 'row',
      justifyContent: 'center',
      marginBottom: 5,
      alignItems: 'center',
      padding:60
    },
    colum: {
      flexDirection:'column', alignSelf:'stretch'
    },
    button: {
      width: ancho*0.5,
      alignSelf: "center",
      
    },
    touch:{
      alignSelf: "center",
      height: 40,
      borderRadius: 6, 
      backgroundColor: '#dcdcdc',
      width: ancho*0.3,
      margin:15,
      flexDirection: 'row',alignItems: 'center'
    },
    input:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'#dcdcdc',
    },
    inputmodal:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      backgroundColor:'#dcdcdc',
      textAlign: 'center',
      fontSize: 15,
      borderWidth: 2,
    },

    inputref:{
      height: 40,
      borderRadius: 6,
      width:ancho, 
      borderWidth: 2,
      backgroundColor:'#dcdcdc',
      
    },
    date:{
      textAlign: 'center', // <-- the magic
      fontWeight: 'bold',
      fontSize: 15,
      textAlignVertical: "center",
      width: '100%'
    },
    text:{
      fontSize: 15,
      margin:5
    },
    texterror:{
      color:'#b22222'
    },
    buttoncolor:{
      backgroundColor:'#000080',
      height: 40,
      borderRadius: 6,
      flexDirection: 'row',alignItems: 'center' 
    },
    textbuton:{
      textAlign: 'center', // <-- the magic
      fontWeight: 'bold',
      fontSize: 15,
      textAlignVertical: "center",
      width: '100%',
      color: '#ffffff'
    }
  })
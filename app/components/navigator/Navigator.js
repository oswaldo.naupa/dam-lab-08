import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View, TextInput, Image,Alert,Button} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import TransferenceFirst from '../transferences/TransferenceFirst'
import TransferenceSecond from '../transferences/TransferenceSecond';
import TransferenceThird from '../transferences/TransferenceThird';
import Maps from '../maps/Maps'


function HomeScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
      </View>
    );
  }
  function DetailsScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
        <Button
          title="Go back to first screen in stack"
          onPress={() => navigation.popToTop()}
        />
      </View>
    );
  }
  function MapsScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Maps></Maps>
      </View>
    );
  }
  const Stack = createStackNavigator();
  
  const Tab = createMaterialBottomTabNavigator();
  
  const TransferenceStack = createStackNavigator();
  
  function TranferenceStackScreen() {
    return(
      <TransferenceStack.Navigator>
        <TransferenceStack.Screen name="First" component={TransferenceFirst}
          options={{
            title: 'FORMULARIO',
            headerRight:()=><Image source={require('../../../img/icono.png') } style={[{width:150, height: 50,margin:10}]}/>,
            headerStyle: {
              backgroundColor: '#000080',
            },
            headerTintColor: '#fff',
            headerTitleStyle: {
              fontWeight: 'bold',
            },
          }}/>
        <TransferenceStack.Screen name="Second" component={TransferenceSecond}
        options={{
          title: 'CONFIRMACION',
          headerRight:()=><Image source={require('../../../img/icono.png') } style={[{width:150, height: 50,margin:10}]}/>,
          headerStyle: {
            backgroundColor: '#000080',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}/>
        <TransferenceStack.Screen name="Third" component={TransferenceThird}
        options={{
          title: 'DATOS ENVIADOS',
          headerRight:()=><Image source={require('../../../img/icono.png') } style={[{width:150, height: 50,margin:10}]}/>,
          headerStyle: {
            backgroundColor: '#000080',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}/>
      </TransferenceStack.Navigator>
    )
  }
  
  export default class Navigator extends Component {
    constructor (props) {
      super(props);
      this.state ={
        textValue: '',
        count: 0,
      };
    }
  
    changeTextInput = text => {
      console.log(text)
      this.setState ({textValue: text});
    };
    onPress = () => {
      this.setState({
        count: this.state.count + 1,
      });
    };
  
    showAlert = () =>{
      Alert.alert(
        'Titulo',
        'Mensaje',
        [
          {
            text:'Cancel',
            onPress: () => console.log('Cancel Pressed'),
            style:'cancel',
          },
          {text:'OK', onPress:() => console.log('OK Pressed')},
        ],
        {cancelable: false},
      );
    };
  
    render() {
      return (
        <NavigationContainer>
        <Tab.Navigator
          initialRouteName="Home"
          tabBarOptions={{
            activeTintColor: '#e91e63',
          }}
          barStyle={{ backgroundColor: '#000080'}}>
          <Tab.Screen 
            name="Home" 
            component={HomeScreen} 
            options = {{
              tabBarLabel: 'Home',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="home" color={color} size={size}/>
              ),
            }}/>
          <Tab.Screen 
            name="Transference" 
            component={TranferenceStackScreen} 
            options = {{
              tabBarLabel: 'Transference',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="home" color={color} size={size}/>
              ),
            }}/>
          <Tab.Screen 
            name="Maps" 
            component={MapsScreen} 
            options = {{
              tabBarLabel: 'Location',
              tabBarIcon: ({color, size}) => (
                <MaterialCommunityIcons name="map" color={color} size={size}/>
              ),
            }}/>
        </Tab.Navigator>
      </NavigationContainer>
      );
    }
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      padding: 10,
    },
    text: {
      alignItems: 'center',
      padding: 10,
    },
    button: {
      top: 10,
      alignItems: 'center',
      backgroundColor: '#DDDDDD',
      padding: 10,
    },
    countContainer: {
      alignItems: 'center',
      padding: 10,
    },
    countText: {
      color: '#FF00FF',
    },
  })